#Name: AppliedEnergistics.zs
#Author: AevumKairos and chadthe2nd
print("Loading Applied Energistics tweaks...");
//Nerf the controller
recipes.remove(<appliedenergistics2:controller>);
recipes.addShaped(<appliedenergistics2:controller>, [
	[<appliedenergistics2:material:24>, <refinedstorage:controller>, <appliedenergistics2:material:24>],
	[<appliedenergistics2:energy_acceptor>, <techreborn:machine_frame:2>, <appliedenergistics2:energy_acceptor>],
	[<quantumflux:quibitcluster:3>, <rftools:modular_storage>, <quantumflux:quibitcluster:3>]]);
recipes.addShaped(<appliedenergistics2:sky_stone_block>, [
  [<minecraft:concrete:15>, <minecraft:concrete:15>, <minecraft:concrete:15>],
  [<minecraft:concrete:15>, <appliedenergistics2:material:45>, <minecraft:concrete:15>],
  [<minecraft:concrete:15>, <minecraft:concrete:15>, <minecraft:concrete:15>]
]);
print("Finished loading Applied Energistics tweaks.");
