#Name: ProgressionOverrides.zs
#Author: AevumKairos and chadthe2nd
print("Loading progression override tweaks...");

#Allows stone hammer to be crafted
recipes.addShaped(<exnihilocreatio:hammer_stone>, [
  [null, <ore:cobblestone>, null],
  [null, <ore:rodStone>, <ore:cobblestone>],
  [<ore:rodStone>, null, null]
]);
recipes.removeByRecipeName("minecraft:oak_planks");
recipes.removeByRecipeName("minecraft:stick");
recipes.removeByRecipeName("extrautils2:shortcut_stick");
recipes.removeByRecipeName("minecraft:spruce_planks");
recipes.removeByRecipeName("minecraft:birch_planks");
recipes.removeByRecipeName("minecraft:jungle_planks");
recipes.removeByRecipeName("minecraft:acacia_planks");
recipes.removeByRecipeName("minecraft:dark_oak_planks");
recipes.removeByRecipeName("integrateddynamics:menril_planks");
recipes.removeByRecipeName("thaumcraft:planksilverwood");
recipes.removeByRecipeName("thaumcraft:plankgreatwood");
recipes.removeByRecipeName("harvestcraft:minecraft_planks_meta_3_x4_pampaperbark");
recipes.removeByRecipeName("harvestcraft:minecraft_planks_meta_3_x4_pamcinnamon");

recipes.addShaped(<minecraft:stick> * 2, [
  [<ore:plankWood>],
  [<ore:plankWood>]
]);
recipes.addShaped(<minecraft:stick> * 4, [
  [<ore:logWood>],
  [<ore:logWood>]
]);
recipes.addShapeless(<minecraft:planks:0> * 2,[<minecraft:log:0>]);
recipes.addShapeless(<minecraft:planks:1> * 2,[<minecraft:log:1>]);
recipes.addShapeless(<minecraft:planks:2> * 2,[<minecraft:log:2>]);
recipes.addShapeless(<minecraft:planks:3> * 2,[<minecraft:log:3>]);
recipes.addShapeless(<minecraft:planks:4> * 2,[<minecraft:log:4>]);
recipes.addShapeless(<minecraft:planks:5> * 2,[<minecraft:log:5>]);
recipes.addShapeless(<minecraft:planks:3> * 2,[<harvestcraft:pampaperbark>]);
recipes.addShapeless(<minecraft:planks:3> * 2,[<harvestcraft:pamcinnamon>]);
recipes.addShapeless(<randomthings:spectreplank> * 2,[<randomthings:spectrelog>]);
recipes.addShapeless(<integrateddynamics:menril_planks> * 2,[<integrateddynamics:menril_log>]);
recipes.addShapeless(<thaumcraft:plank_silverwood> * 2,[<thaumcraft:log_silverwood>]);
recipes.addShapeless(<thaumcraft:plank_greatwood> * 2,[<thaumcraft:log_greatwood>]);
print("Finished loading progression override tweaks.");
