#Name: Powersuits.zs
#Author: AevumKairos and chadthe2nd
print("Loading MachineMuse's Modular Powersuits tweaks...");

mods.jei.JEI.removeAndHide(<powersuits:powerarmorcomponent:18>); // Removes solar panel for stoneblock reasons

print("Finished loading MachineMuse's Modular Powersuits tweaks.");
