#loader contenttweaker
import mods.contenttweaker.VanillaFactory;
import mods.contenttweaker.Block;

var wubBlock = VanillaFactory.createBlock("wubblock", <blockmaterial:iron>);
wubBlock.setBlockHardness(7.0);
wubBlock.setBlockSoundType(<soundtype:snow>);
wubBlock.setLightOpacity(3);
wubBlock.setLightValue(0);
wubBlock.register();