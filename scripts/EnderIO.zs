#Name: EnderIO.zs
#Author: AevumKairos and chadthe2nd
print("Loading Ender IO tweaks...");


mods.jei.JEI.removeAndHide(<enderio:item_material:3>); //Photovoltaic Plate            All of these are useless in StoneBlock
mods.jei.JEI.removeAndHide(<enderio:block_solar_panel:0>); // Simple Photovoltaic Cell
mods.jei.JEI.removeAndHide(<enderio:block_solar_panel:1>); // Photovoltaic Cell
mods.jei.JEI.removeAndHide(<enderio:block_solar_panel:2>); // Advanced Photovoltaic Cell
mods.jei.JEI.removeAndHide(<enderio:block_solar_panel:3>); // Vibrant Photovoltaic Cell
recipes.removeByRecipeName("enderio:simple_chassis");
recipes.removeByRecipeName("enderio:gear_iron");

//Simple chassis
recipes.addShaped(<enderio:item_material:0>, [
  [<minecraft:iron_bars>, <contenttweaker:wubblock>, <minecraft:iron_bars>],
  [<appliedenergistics2:sky_stone_block>, <enderio:item_material:20>, <appliedenergistics2:sky_stone_block>],
  [<minecraft:iron_bars>, <minecraft:diamond>, <minecraft:iron_bars>]
]);

//Infinity gear
recipes.addShaped(<enderio:item_material:11>, [
  [<ore:ingotBronze>, <ore:ingotIron>, <ore:ingotBronze>],
  [<ore:ingotIron>, <ore:dustBedrock>, <ore:ingotIron>],
  [<ore:ingotBronze>, <ore:ingotIron>, <ore:ingotBronze>]
]);
print("Finished loading Ender IO tweaks.");
