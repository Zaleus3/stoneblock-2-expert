#Name: ExtraUtilities2.zs
#Author: AevumKairos and chadthe2nd
print("Loading Extra Utilities 2 tweaks...");

mods.jei.JEI.removeAndHide(<extrautils2:passivegenerator:0>); // Removes solar panel for stoneblock reasons

print("Finished loading Extra Utilities 2 tweaks.");
