#Name: StorageDrawers.zs
#Author: AevumKairos and chadthe2nd

print("Loading Storage Drawers tweaks...");

recipes.remove(<storagedrawers:controller>);
recipes.addShaped(<storagedrawers:controller>, [
  [<enderio:item_alloy_ingot:9>, <enderio:block_alloy:4>, <enderio:item_alloy_ingot:9>],
  [<storagedrawers:controllerslave>, <storagedrawers:compdrawers>, <storagedrawers:controllerslave>],
  [<enderio:item_alloy_ingot:9>, <enderio:block_alloy:4>, <enderio:item_alloy_ingot:9>]
]);

print("Finished loading Storage Drawers tweaks...");
