#Name: Mekanism.zs
#Author: AevumKairos and chadthe2nd
print("Loading Mekanism tweaks...");

mods.jei.JEI.removeAndHide(<mekanismgenerators:solarpanel>); // Removed solar panels for stoneblock reasons

//Remove steel, potentially temporary.
mods.mekanism.infuser.removeRecipe(<mekanism:enrichediron>);
mods.mekanism.infuser.removeRecipe(<ore:dustSteel>);
print("Finished loading Mekanism tweaks.");
