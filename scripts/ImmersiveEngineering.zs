#Name: ImmersiveEngineering.zs
#Author: AevumKairos and chadthe2nd

/**/
print("Loading Immersive Engineering tweaks...");

recipes.remove(<immersiveengineering:metal_decoration0:3>); //Redstone Engineering Block
recipes.addShaped(<immersiveengineering:metal_decoration0:3> * 2, [
  [<enderio:item_alloy_ingot:0>, <minecraft:redstone_block:0>, <enderio:item_alloy_ingot:0>],
  [<minecraft:redstone_block:0>, <immersiveengineering:metal_decoration1:1>, <minecraft:redstone_block:0>],
  [<enderio:item_alloy_ingot:0>, <minecraft:redstone_block:0>, <enderio:item_alloy_ingot:0>]
]);
recipes.remove(<immersiveengineering:metal_decoration0:4>);  //Light Engineering Block
recipes.addShaped(<immersiveengineering:metal_decoration0:4> * 2, [
  [<enderio:item_alloy_ingot:0>, <immersiveengineering:material:9>, <enderio:item_alloy_ingot:0>],
  [<ore:ingotBronze>, <immersiveengineering:metal_decoration1:1>, <ore:ingotBronze>],
  [<enderio:item_alloy_ingot:0>, <immersiveengineering:material:9>, <enderio:item_alloy_ingot:0>]
]);
recipes.remove(<immersiveengineering:metal_decoration0:5> * 2);//Heavy Engineering Block
recipes.addShaped(<immersiveengineering:metal_decoration0:5>, [
  [<ore:ingotDarkSteel>, <immersiveengineering:metal_decoration0:4>, <ore:ingotWub>],
  [<minecraft:piston>, <ore:itemSimpleMachineChassi>, <minecraft:piston>],
  [<ore:ingotWub>, <ore:blockLead>, <ore:ingotDarkSteel>]
]);
recipes.removeByRecipeName("immersiveengineering:material/stick_steel"); //Steel rod nerf
recipes.addShaped(<immersiveengineering:material:2>, [
  [<ore:ingotSteel>],
  [<ore:ingotSteel>],
  [<ore:ingotSteel>]
]);
recipes.remove(<minecraft:hopper>);
recipes.removeByRecipeName("uppers:upper");
recipes.addShaped(<minecraft:hopper>, [
  [<ore:plateIron>, null, <ore:plateIron>],
  [<ore:plateIron>, <minecraft:chest>, <ore:plateIron>],
  [null, <ore:plateIron>, null]
]);
recipes.remove(<immersiveengineering:metal_device1:2>); //Kinetic Dynamo
recipes.addShaped(<immersiveengineering:metal_device1:2>, [
  [null, null, null],
  [<minecraft:redstone>, <immersiveengineering:metal_decoration0>, <minecraft:redstone>],
  [<ore:ingotConductiveIron>, <ore:ingotConductiveIron>, <ore:ingotConductiveIron>]
]);
recipes.remove(<immersiveengineering:metal_device1:1>);  //External Heater
recipes.addShaped(<immersiveengineering:metal_device1:1>, [
  [<ore:ingotConductiveIron>, <ore:ingotConstantan>, <ore:ingotConductiveIron>],
  [<ore:ingotConstantan>, <immersiveengineering:metal_decoration0>, <ore:ingotConstantan>],
  [<ore:ingotConductiveIron>, <minecraft:redstone>, <ore:ingotConductiveIron>]
]);
recipes.remove(<immersiveengineering:stone_decoration:10>); // Alloy Kiln
recipes.addShaped(<immersiveengineering:stone_decoration:10> * 2, [
  [<ore:sandstone>, <minecraft:brick>, <ore:sandstone>],
  [<minecraft:brick>, <ore:sandstone>, <minecraft:brick>],
  [<ore:sandstone>, <minecraft:brick>, <ore:sandstone>]
]);
recipes.remove(<immersiveengineering:material:1>); //Iron rod
mods.immersiveengineering.MetalPress.removeRecipe(<immersiveengineering:material:1>);
mods.immersiveengineering.MetalPress.addRecipe(<immersiveengineering:material:1>, <minecraft:iron_ingot>, <immersiveengineering:mold:2>, 10000);
recipes.addShaped(<immersiveengineering:material:1>, [
  [<null>, <null>, <null>],
  [<null>, <minecraft:iron_ingot>, <null>],
  [<null>, <minecraft:iron_ingot>, <null>]
]);
recipes.addShaped(<immersiveengineering:material:1>, [
  [<null>, <null>, <null>],
  [<null>, <null>, <minecraft:iron_ingot>],
  [<null>, <null>, <minecraft:iron_ingot>]
]);
recipes.addShaped(<immersiveengineering:material:1>, [
  [<null>, <null>, <null>],
  [<minecraft:iron_ingot>, <null>, <null>],
  [<minecraft:iron_ingot>, <null>, <null>]
]);

recipes.remove(<immersiveengineering:stone_decoration:0>);  //Coke brick
recipes.addShaped(<immersiveengineering:stone_decoration:0> * 3, [
  [<minecraft:concrete:15>, <ore:ingotBrick>, <minecraft:concrete:15>],
  [<ore:ingotBrick>, <ore:sandstone>, <ore:ingotBrick>],
  [<minecraft:concrete:15>, <ore:ingotBrick>, <minecraft:concrete:15>]
]);

print("Finished loading Immersive Engineering tweaks...");
