#Name: ProgressionOverrides.zs
#Author: AevumKairos and chadthe2nd
print("Loading base game tweaks...");

//Bucket override because expert
recipes.remove(<minecraft:bucket>);
recipes.addShaped(<minecraft:bucket>, [
  [null, null, null],
  [<ore:plateIron>, null, <ore:plateIron>],
  [null, <ore:plateIron>, null]
]);
recipes.remove(<minecraft:piston>);
recipes.addShaped(<minecraft:piston>, [
  [<ore:plankTreatedWood>, <ore:plankTreatedWood>, <ore:plankTreatedWood>],
  [<minecraft:cobblestone>, <ore:stickIron>, <minecraft:cobblestone>],
  [<minecraft:cobblestone>, <deepmoblearning:soot_covered_redstone>, <minecraft:cobblestone>]
]);

recipes.remove(<minecraft:iron_bars>);
recipes.addShaped(<minecraft:iron_bars> * 4, [
  [<ore:ingotIron>, <ore:stickIron>, <ore:ingotIron>],
  [<ore:stickIron>, <ore:stickIron>, <ore:stickIron>],
  [<ore:ingotIron>, <ore:stickIron>, <ore:ingotIron>]
]);

print("Finished loading base game tweaks.");
