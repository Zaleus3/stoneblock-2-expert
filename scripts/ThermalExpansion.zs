#Name: ThermalExpansion.zs
#Author: AevumKairos and chadthe2nd

print("Loading Thermal Expansion tweaks...");
//Remove steel
mods.thermalexpansion.InductionSmelter.removeRecipe(<ore:dustCoal>.firstItem * 4, <ore:dustIron>.firstItem);
mods.thermalexpansion.InductionSmelter.removeRecipe(<ore:dustCharcoal>.firstItem * 4, <ore:dustIron>.firstItem);
mods.thermalexpansion.InductionSmelter.removeRecipe(<ore:dustCharcoal>.firstItem * 4, <ore:ingotIron>.firstItem);
mods.thermalexpansion.InductionSmelter.removeRecipe(<ore:fuelCoke>.firstItem, <ore:dustIron>.firstItem);
mods.thermalexpansion.InductionSmelter.removeRecipe(<ore:fuelCoke>.firstItem, <ore:ingotIron>.firstItem);


print("Finished loading Thermal Expansion tweaks.");
