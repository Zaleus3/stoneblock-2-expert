#Name: RefinedStorage.zs
#Author: AevumKairos and chadthe2nd
import crafttweaker.item.IItemStack as IItemStack;
print("Loading Refined Storage tweaks...");
//Remove most functionality
val ItemsToHide = [
<refinedstorage:cover>,
<refinedstorage:wireless_fluid_grid:1>,
<refinedstorage:wireless_grid:1>,
<refinedstorage:wireless_crafting_monitor:1>,
<refinedstorage:storage_disk:4>,
<refinedstorage:fluid_storage_disk:4>,
<refinedstorage:hollow_cover>,
<refinedstorage:crafter_manager>,
<refinedstorage:network_transmitter>,
<refinedstorage:network_receiver>,
<refinedstorage:storage>,
<refinedstorage:crafter>,
<refinedstorage:controller:1>,
<refinedstorage:portable_grid>,
<refinedstorage:portable_grid:1>,
<refinedstorage:grid:2>,
<refinedstorage:fluid_storage>,
<refinedstorage:exporter>,
<refinedstorage:external_storage>,
<refinedstorage:constructor>,
<refinedstorage:destructor>,
<refinedstorage:reader>,
<refinedstorage:writer>,
<refinedstorage:crafting_monitor>,
<refinedstorage:security_manager>,
<refinedstorage:detector>,
<refinedstorage:relay>,
<refinedstorage:interface>,
<refinedstorage:fluid_interface>,
<refinedstorage:wireless_transmitter>,
<refinedstorage:crafter_manager>,
<refinedstorage:wireless_fluid_grid>,
<refinedstorage:wireless_crafting_monitor>,
<refinedstorage:upgrade:1>,
<refinedstorage:upgrade:3>,
<refinedstorage:upgrade:6>,
<refinedstorage:upgrade:7>,
<refinedstorage:upgrade:8>,
<refinedstorage:upgrade:9>,
<refinedstorage:filter>,
<refinedstorage:pattern>,
<refinedstorage:network_card>,
<refinedstorage:security_card>,
<refinedstorage:storage:1>,
<refinedstorage:storage:2>,
<refinedstorage:storage:3>,
<refinedstorage:storage:4>,
<refinedstorage:fluid_storage:1>,
<refinedstorage:fluid_storage:2>,
<refinedstorage:fluid_storage:3>,
<refinedstorage:fluid_storage:4>,
<refinedstorage:fluid_storage_disk:0>,
<refinedstorage:fluid_storage_disk:1>,
<refinedstorage:fluid_storage_disk:2>,
<refinedstorage:fluid_storage_disk:3>,
<refinedstorage:fluid_storage_part:0>,
<refinedstorage:fluid_storage_disk:1>,
<refinedstorage:fluid_storage_disk:2>,
<refinedstorage:fluid_storage_disk:3>,
<refinedstorage:grid:3>
] as IItemStack[];

for item in ItemsToHide {
mods.jei.JEI.removeAndHide(item);
}

//Begin nerfing of recipes

//Controller
recipes.remove(<refinedstorage:controller>);
recipes.addShaped("Refined Storage Controller Enderfied", <refinedstorage:controller>, [
  [<refinedstorage:quartz_enriched_iron>, <refinedstorage:processor:5>, <refinedstorage:quartz_enriched_iron>],
  [<enderio:item_alloy_ingot:5>, <enderio:item_material:0>, <enderio:item_alloy_ingot:5>],
  [<refinedstorage:quartz_enriched_iron>, <refinedstorage:processor:5>, <refinedstorage:quartz_enriched_iron>]
]);

//Quartz enriched iron (defined in expert_additions.xml)
//recipes.removeByRecipeName("refinedstorage:quartz_enriched_iron");
recipes.removeShaped(<refinedstorage:quartz_enriched_iron> * 4, [[<minecraft:iron_ingot>, <minecraft:iron_ingot>], [<minecraft:iron_ingot>, <minecraft:quartz>]]);

print("Finished loading Refined Storage tweaks.");
