#Name: TConstruct.zs
#Author: AevumKairos and chadthe2nd
import crafttweaker.liquid.ILiquidDefinition;

print("Loading TConstruct tweaks...");

mods.tconstruct.Alloy.removeRecipe(<liquid:steel>);
val wub = <liquid:vasholine>.definition;  //wub-wub juice
wub.temperature = 1500;
wub.luminosity = 20;
val lava = <liquid:lava>.definition;
lava.temperature = 50;
mods.tconstruct.Fuel.registerFuel(<liquid:vasholine> * 5, 300);  //wub-wub juice as fuel
recipes.addShaped(<contenttweaker:wubblock>, [
  [<tp:wub_gem>, <tp:wub_gem>, <tp:wub_gem>],
  [<tp:wub_gem>, <tp:wub_gem>, <tp:wub_gem>],
  [<tp:wub_gem>, <tp:wub_gem>, <tp:wub_gem>]
]);
recipes.removeByRecipeName("tp:wub_juice");  //remove ez juice
mods.tconstruct.Casting.removeTableRecipe(<enderio:item_material:11>);  //nice try, easy gear
print("Finished loading TConstruct tweaks.");
