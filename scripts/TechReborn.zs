#Name: TechReborn.zs
#Author: AevumKairos and chadthe2nd

print("Loading Tech Reborn tweaks...");

recipes.remove(<techreborn:quantum_chest>); // Crashes when trying to open gui
recipes.remove(<techreborn:creative_quantum_chest>); // Didn't want ot take a chance of the previous item doing same thing
mods.jei.JEI.removeAndHide(<techreborn:solar_panel:0>); // Disabling solar panels for stoneblock reasons
mods.jei.JEI.removeAndHide(<techreborn:solar_panel:1>);
mods.jei.JEI.removeAndHide(<techreborn:solar_panel:2>);
mods.jei.JEI.removeAndHide(<techreborn:solar_panel:3>);
mods.jei.JEI.removeAndHide(<techreborn:solar_panel:4>);
mods.jei.JEI.removeAndHide(<techreborn:creative_solar_panel>);
mods.techreborn.rollingMachine.removeRecipe(<minecraft:iron_bars>);
print("Finished loading Tech Reborn tweaks.");
