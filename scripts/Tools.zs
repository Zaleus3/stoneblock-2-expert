#Name: RefinedStorage.zs
#Author: AevumKairos and chadthe2nd
import crafttweaker.item.IItemStack as IItemStack;
print("Loading tool tweaks...");
val toolRemove = [
<thermalfoundation:tool.pickaxe_copper>,
<thermalfoundation:tool.pickaxe_tin>,
<thermalfoundation:tool.pickaxe_silver>,
<thermalfoundation:tool.pickaxe_lead>,
<thermalfoundation:tool.pickaxe_aluminum>,
<thermalfoundation:tool.pickaxe_nickel>,
<thermalfoundation:tool.pickaxe_platinum>,
<thermalfoundation:tool.pickaxe_steel>,
<thermalfoundation:tool.pickaxe_electrum>,
<thermalfoundation:tool.pickaxe_invar>,
<thermalfoundation:tool.pickaxe_bronze>,
<thermalfoundation:tool.pickaxe_constantan>,
<mysticalagradditions:inferium_paxel>,
<mysticalagradditions:prudentium_paxel>,
<mysticalagradditions:intermedium_paxel>,
<mysticalagradditions:superium_paxel>,
<mysticalagradditions:supremium_paxel>,
<immersiveengineering:pickaxe_steel>,
<immersiveengineering:shovel_steel>,
<immersiveengineering:axe_steel>,
<immersiveengineering:sword_steel>,
<mysticalagriculture:inferium_pickaxe>,
<mysticalagriculture:prudentium_pickaxe>,
<mysticalagriculture:intermedium_pickaxe>,
<mysticalagriculture:superium_pickaxe>
		] as IItemStack[];
    for tool in toolRemove {
  		mods.jei.JEI.removeAndHide(tool);
  	}
val toolNerf = [
    <minecraft:golden_axe>,
    <minecraft:golden_shovel>,
    <minecraft:golden_hoe>,
    <minecraft:golden_pickaxe>,
    <minecraft:golden_sword>,
    <minecraft:diamond_shovel>,
    <minecraft:diamond_axe>,
    <minecraft:diamond_pickaxe>,
    <minecraft:diamond_hoe>,
    <minecraft:diamond_sword>,
    <minecraft:iron_shovel>,
    <minecraft:iron_axe>,
    <minecraft:iron_pickaxe>,
    <minecraft:iron_hoe>,
    <minecraft:iron_sword>,
    <minecraft:wooden_pickaxe>,
    <minecraft:wooden_sword>,
    <minecraft:wooden_hoe>,
    <minecraft:wooden_shovel>,
    <minecraft:wooden_axe>,
    <minecraft:stone_pickaxe>,
    <minecraft:stone_hoe>,
    <minecraft:stone_axe>,
    <minecraft:stone_sword>,
    <minecraft:stone_shovel>
    ] as IItemStack[];

    for tool in toolNerf {
      tool.maxDamage = 1;
    }

print("Finished loading tool tweaks...");
