#Name: ActuallyAdditions.zs
#Author: AevumKairos and chadthe2nd
print("Loading Actually Additions tweaks...");

mods.jei.JEI.removeAndHide(<actuallyadditions:block_furnace_solar>); // Removes solar panel for stoneblock reasons

print("Finished loading Actually Additions tweaks.");
