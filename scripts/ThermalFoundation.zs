#Name: ThermalFoundation.zs
#Author: AevumKairos and chadthe2nd

print("Loading Thermal Foundation tweaks...");


val itemDef = <thermalfoundation:material>.definition;


 //does this for <thermalfoundation:material:22> to <thermalfoundation:material:27>; 2nd block is for 256 to 264; 3rd block is for 288 to 295
for i in 22 to 28{
    recipes.remove(itemDef.makeStack(i));
}

for i in 256 to 265{
    recipes.remove(itemDef.makeStack(i));
}

for i in 288 to 296{
    recipes.remove(itemDef.makeStack(i));
}

recipes.removeByRecipeName("redstonearsenal:material_4"); // fluxed electrum gear, put in here because it's a gear

print("Finished loading Thermal Foundation tweaks.");
